## Aplikacja do zarządzania kartami

---

## Instrukcja
Po pobraniu repozytorium należy wykonać następujące komendy:

1. 'yarn install' - w celu pobrania wszystkich Dependencies 
2. 'yarn start' - uruchamia devServer i odpala aplikacje w nowej zakładce pod lokalnym adresem http://localhost:8080/
3. (Opcjonalnie) 'yarn build' - komenda generuje finalne pliki