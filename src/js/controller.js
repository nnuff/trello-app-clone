export default class HomeController {
    constructor($scope, homeService) {
      this.service = homeService;
      this.$scope = $scope;
      this.$scope.title = 'Trello';
      this.$scope.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    }

    $onInit() {
      this.service.refreshData();
    }

  }