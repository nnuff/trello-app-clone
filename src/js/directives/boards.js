//var templateUrl = require('ngtemplate!html!./boards.html');

export default class BoardDirective {
    constructor() {
        this.template = `
      <div class="row">
        <div class="col-4" ng-repeat="list in ctrl.My_Board">
            <div class="list-group-item list-group-item-action active">{{list.name}}</div> 
                <div class="list-group bg-light" dragula-model="list.cards" dragula='"evented-bag"' dragula-scope="$parent" id="{{list.id}}">
                    <div class="py-2 px-2" ng-repeat="card in list.cards" id="{{card.shortLink}}">
                        <div class="card">
                            <div class="row align-items-center no-gutters" ng-hide="card.modeOn" >
                            <div class="col-8">
                                <div class="card-body">
                                <h6 class="card-title">{{card.name}}</h6>
                                <p class="card-text">{{card.desc}}</p>
                                </div>
                            </div>
                            <div class="col">
                                <div class="text-right">
                                    <a class="btn btn-link" href="#" ng-hide="card.modeOn" ng-click="ctrl.openCardEditor(card)"><i class="fa fa-edit fa-lg"></i></a>
                                    <a class="btn btn-link" href="#" ng-hide="card.modeOn" ng-click="ctrl.deleteCard(card.id)"><i class="fa fa-trash fa-lg"></i></a>
                                </div>
                            </div>
                        </div>
                       
                        <form class="py-2 px-2" ng-show="card.modeOn" name="editorForm" ng-submit="ctrl.updateCardSubmit(editorForm.$valid, card, card.name, card.desc, card.id)" novalidate>
                            <div class="form-group">
                                <label for="card_name">Name</label>
                                <input ng-change="ctrl.updateValue(card.name, card_name)" name="card_name" ng-init="card_name = card.name" class="form-control" type="text" ng-model="card.name" required>
                            </div>
                            <div class="form-group">
                                <label for="card_desc">Description</label>
                                <textarea ng-change="ctrl.updateValue(card.desc, card_desc)" name="card_desc" ng-init="card_desc = card.desc" class="form-control" type="text" ng-model="card.desc"></textarea>
                            </div>
                            <button class="btn btn-primary" type="submit" ng-disabled="editorForm.$invalid" />Save</button>
                            <a class="btn btn-outline-primary" href="#" ng-click="ctrl.closeForm(card, card_name, card_desc)" role="button"><i class="fa fa-times"></i></a>
                        </form>

                        </div>
                    </div>
            </div>
            <div class="py-2 px-2 bg-light" ng-show="list.modeOn">
                <form name="composeForm" ng-submit="ctrl.addNewCardSubmit(composeForm.$valid, list, card_name, list.id)" novalidate>
                    <div class="form-group">
                        <textarea name="card_name" placeholder="Provide a title" class="form-control" type="text" ng-model="card_name" required></textarea>
                    </div>
                    <button class="btn btn-primary" type="submit" ng-disabled="composeForm.$invalid" />Add a card</button>
                    <a class="btn btn-outline-primary" href="#" ng-click="ctrl.closeForm(list)" role="button"><i class="fa fa-times"></i></a>
                </form>
            </div>
            <div class="py-2 px-2 bg-light">
                <button type="button" ng-click="ctrl.newCardForm(list)" class="btn btn-light btn-block" ng-if="$first"><i class="fa fa-plus"></i> Add another card</button>
            </div>
        </div>
      </div>`;
        this.restrict = 'E';
        this.scope = {};
        this.controller = BoardDirCtrl;
        this.controllerAs = 'ctrl';
        this.bindToController = true;
    }

    // Directive compile function
    compile() {
    }

    // Directive link function
    link() {
    }
}

// Directive's controller
class BoardDirCtrl {
    constructor($scope, homeService, dragulaService) {
        this.service = homeService;
        this.$scope = $scope;

        homeService.refreshData().then((resp) => {
           this.My_Board = resp;
        })

        this.dragulaService = dragulaService;
        
        this.openCardEditor = (card) => {
            card.modeOn = true;
        }

        this.updateValue = (newValue, initValue) => {
             if(newValue !== initValue) {
                 this.gotChanged = true;
             }
         }

        this.updateCardSubmit = (isValid, card, name, desc, card_id) => {
            if (isValid && this.gotChanged) {
                this.service.updateCard(card_id, name, desc);
                this.gotChanged = false;
            } 
            card.modeOn = false;
        }  

        this.refreshData = () => {
            homeService.refreshData().then((resp) => {
               this.My_Board = resp;
            })
        };

        this.addNewCardSubmit = (isValid, list, name, list_id) => {
            if (isValid) {
                this.service.addNew(name, list_id).then((resp) => {
                    this.refreshData();
                });
                list.modeOn = false;
            } 
        };

        this.deleteCard = (card_id) => {
           homeService.deleteCard(card_id).then((resp) => {
                this.refreshData();
           });
        };

        this.closeForm = (el, card_name, card_des) => {

            if(card_name || card_des) {
                el.name = card_name;
                el.desc = card_des;
            }

            el.modeOn = false;
        };


        this.newCardForm = (list) => {
            list.modeOn = true;
        };

        $scope.$on('evented-bag.drop', function (e, el, target) {
            let card_id = el[0].id;
            let list_id = target[0].id;
            homeService.moveCard(card_id, list_id);
        });

    }
}