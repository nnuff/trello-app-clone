import 'bootstrap';
import '../sass/style.scss';

import angular from 'angular';
import home from './module';

angular.module('app', [home]);