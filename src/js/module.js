import angular from 'angular';

import angularDragula from 'angularjs-dragula';

import HomeController from './controller';
import homeService from './service';
import boardDirective from './directives/boards';

const module = angular.module('home', [angularDragula(angular)])
.controller('HomeController', HomeController)
.service('homeService', homeService)
.directive('boardDirective', () => new boardDirective).name;

export default module;
