export default class homeService {
    constructor($http) {
      this.$http = $http;
      
      this.token = '315e6d02803ffd1eda6f439d73f7b15e6cdae16f235fddbce5e2c927ca420c66';
      this.key = 'b1b6fa5f83766def2ede8375208764a1';
      this.board_id = '5da8dfa0b7c20e0a09ad1e62';     
    }

    refreshData() {
      return this.$http.get(`https://api.trello.com/1/boards/${this.board_id}/lists?cards=all&card_fields=all&key=${this.key}&token=${this.token}`).then((res) => res.data).catch((err) => console.error(err));
   }

    deleteCard(card_id) {
      return this.$http.delete(`https://api.trello.com/1/cards/${card_id}?key=${this.key}&token=${this.token}`);
    }

    updateCard(card_id, name, desc) {
      let params = {
        "desc" : desc,
        "name" : name,
        "key": this.key,
        "token": this.token
      }
      return this.$http.put(`https://api.trello.com/1/cards/${card_id}`, params);
    }

    addNew(name = '', list_id) {
      let params = {
        "name" : name,
        "pos": "bottom",
        "idList": list_id,
        "keepFromSource": "all",
        "key": this.key,
        "token": this.token
      }
      return this.$http.post(`https://api.trello.com/1/cards`, params);
    }

    moveCard(card_id, list_id) {
      let params = {
        "idList" : list_id,
        "key": this.key,
        "token": this.token
      }
      return this.$http.put(`https://api.trello.com/1/cards/${card_id}`, params);
    }

  }
  
  homeService.$inject = ['$http'];