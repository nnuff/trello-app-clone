const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  // mode: 'development',
  entry: [
    'babel-polyfill',
    './src/js/main.js'
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: 'main.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
            loader: 'babel-loader'
          }
      },
      {
        test: /\.(woff2?|ttf|otf|eot|svg)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }]
      }, 
      {
        test: /\.scss$/,
        use: ['style-loader',
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
            },
          },
          'postcss-loader',
          'sass-loader']
      }
         ]

  },
  devtool: 'cheap-module-eval-source-map',
  plugins: [
    new CleanWebpackPlugin('dist', {}),
    new HtmlWebpackPlugin({
      title: 'Trello',
      template: './src/index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'style.bundle.css',
    }),
    new CopyWebpackPlugin([
      { from: 'node_modules/font-awesome/fonts', to: 'fonts/'}
    ]),
  ],
  devServer: {
    contentBase: path.resolve(__dirname, "../dist/"),
    open: true
  }

};